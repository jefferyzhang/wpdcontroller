// WPDController.cpp : main project file.

#include "stdafx.h"
#define INITGUID
#include <initguid.h>

using namespace System;
using namespace System::Security::Principal;
using namespace System::Xml;
using namespace System::IO;
using namespace System::Diagnostics;
using namespace System::Text;

void logIt(TCHAR* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	//TCHAR s[2048] = { 0 };
	//_vstprintf_s(s, fmt, args);
	CString s;
	s.FormatV(fmt, args);
	va_end(args);
	//_tprintf(s); _tprintf(_T("\n"));
	OutputDebugString(s);
}

#include <gpedit.h>
/*
SetGroupPolicy(HKEY_CURRENT_USER,
L"Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer",
L"StartMenuLogOff", REG_DWORD, NULL, 1);

Software\Microsoft\Windows\CurrentVersion\Group Policy Objects\{FD4D413B-29F7-428B-90A1-F42814F7FBA7}Machine\Software\Policies\Microsoft\Windows\RemovableStorageDevices\{6AC27878-A6FA-4155-BA85-F98F491D4F33}
Software\Microsoft\Windows\CurrentVersion\Group Policy Objects\{FD4D413B-29F7-428B-90A1-F42814F7FBA7}Machine\Software\Policies\Microsoft\Windows\RemovableStorageDevices\{F33FDC04-D1AC-4E8E-9A30-19BBD4B108AE}

Deny_Write = 1
Deny_Read = 1

SetGroupPolicy(HKEY_CURRENT_USER,_T("Software\\Microsoft\\Windows\\CurrentVersion\\Group Policy Objects\\{FD4D413B-29F7-428B-90A1-F42814F7FBA7}Machine\\Software\\Policies\\Microsoft\\Windows\\RemovableStorageDevices\\{6AC27878-A6FA-4155-BA85-F98F491D4F33}"),
_T("Deny_Write"), REG_DWORD, NULL, 1);

*/
int SetGroupPolicy_WPD_Device(DWORD iMode, DWORD dwkeyValue)
{
#define KEY_COUNT		2
#define DENY_WRITE		_T("Deny_Write")
#define DENY_READ		_T("Deny_Read")
#define KEY_ONEPATH			_T("Software\\Policies\\Microsoft\\Windows\\RemovableStorageDevices\\{6AC27878-A6FA-4155-BA85-F98F491D4F33}")  //
#define KEY_TWOPATH			_T("Software\\Policies\\Microsoft\\Windows\\RemovableStorageDevices\\{F33FDC04-D1AC-4E8E-9A30-19BBD4B108AE}")
	TCHAR* SubKeys[KEY_COUNT] = { KEY_ONEPATH, KEY_TWOPATH };//
	HRESULT hr; 
	HKEY ghKey, ghSubKey[KEY_COUNT];
	LPDWORD flag = NULL;
	IGroupPolicyObject *pGPO = NULL;
	hr = CoCreateInstance(CLSID_GroupPolicyObject, NULL, CLSCTX_INPROC_SERVER, IID_IGroupPolicyObject, (LPVOID*)&pGPO);
	if (!SUCCEEDED(hr))  
	{
		logIt(_T("Failed to initialize GPO\n"));
		return GetLastError();
	}

	if ((hr = pGPO->OpenLocalMachineGPO(GPO_OPEN_LOAD_REGISTRY)) != S_OK)
	{
		logIt(_T("Failed to get the OpenLocalMachineGPO mapping\n"));
		return GetLastError();
	}

	if (pGPO->GetRegistryKey(GPO_SECTION_MACHINE, &ghKey) != S_OK)
	{
		logIt(_T("Failed to get the GetRegistryKey key\n"));
		return GetLastError();
	}

	LSTATUS rStatus=ERROR_ACCESS_DENIED;
	DWORD dwData;
	switch (iMode)
	{
		case 0://Not Configuration
			rStatus = RegDeleteKey(ghKey, SubKeys[0]);
			rStatus = RegDeleteKey(ghKey, SubKeys[1]);
			break;
		case 1://Disable wpd
			for (int i = 0; i < KEY_COUNT; i++)
			{
				if (RegOpenKeyEx(ghKey, SubKeys[i], 0, KEY_WRITE, &ghSubKey[i]) != ERROR_SUCCESS)
				{
					rStatus = RegCreateKeyEx(
						ghKey,
						SubKeys[i],
						0,
						NULL,
						REG_OPTION_NON_VOLATILE,
						KEY_WRITE,
						NULL,
						&ghSubKey[i],
						NULL);
				}

				rStatus = RegSetValueEx(ghSubKey[i], DENY_WRITE, NULL, REG_DWORD, (BYTE *)(&dwkeyValue), sizeof(DWORD));
				rStatus = RegSetValueEx(ghSubKey[i], DENY_READ, NULL, REG_DWORD, (BYTE *)(&dwkeyValue), sizeof(DWORD));

				rStatus = RegCloseKey(ghSubKey[i]);
			}
			break;
		case 2://enable
			for (int i = 0; i < KEY_COUNT; i++)
			{
				BOOL bCreate = FALSE;
				if (RegOpenKeyEx(ghKey, SubKeys[i], 0, KEY_WRITE, &ghSubKey[i]) != ERROR_SUCCESS)
				{
					rStatus = RegCreateKeyEx(
						ghKey,
						SubKeys[i],
						0,
						NULL,
						REG_OPTION_NON_VOLATILE,
						KEY_WRITE,
						NULL,
						&ghSubKey[i],
						NULL);
					bCreate = TRUE;
				}
				DWORD dwType = 0;
				DWORD cbType = sizeof(dwkeyValue);
				if (!bCreate)
				{
					rStatus = RegGetValue(ghKey, SubKeys[i], DENY_WRITE, RRF_RT_ANY, &dwType, (BYTE *)(&dwkeyValue), &cbType);
					if (rStatus != ERROR_SUCCESS) dwData = 0;
					else RegDeleteValue(ghSubKey[i], DENY_WRITE);
					rStatus = RegSetValueEx(ghSubKey[i], DENY_WRITE, NULL, REG_DWORD, (BYTE *)(&dwData), sizeof(DWORD));
					rStatus = RegSetValueEx(ghSubKey[i], DENY_READ, NULL, REG_DWORD, (BYTE *)(&dwData), sizeof(DWORD));
				}
				else
				{
					dwData = 0;
					rStatus = RegSetValueEx(ghSubKey[i], DENY_WRITE, NULL, REG_DWORD, (BYTE *)(&dwData), sizeof(DWORD));
					rStatus = RegSetValueEx(ghSubKey[i], DENY_READ, NULL, REG_DWORD, (BYTE *)(&dwData), sizeof(DWORD));
				}
				rStatus = RegCloseKey(ghSubKey[i]);
			}
			break;
		default:
		{
			logIt(_T(" set mode failed. unknow this mode.\n"));
		}
	}

	if (rStatus != ERROR_SUCCESS)
	{
		logIt(_T("Access registry failed."));
		return rStatus;
	}

	GUID ThisAdminToolGuid =
	{
		0x0F6B957E,
		0x509E,
		0x11D1,
		{ 0xA7, 0xCC, 0x00, 0x00, 0xF8, 0x75, 0x71, 0xE3 }
	};

	GUID RegistryId = REGISTRY_EXTENSION_GUID;
	if (pGPO->Save(TRUE, TRUE, const_cast<GUID*>(&RegistryId), const_cast<GUID*>(&CLSID_GPESnapIn)) != S_OK)
	{
		RegCloseKey(ghKey);
		logIt(_T("Save failed\n"));
		return GetLastError();
	}

	pGPO->Release();
	RegCloseKey(ghKey);
	return ERROR_SUCCESS;
}

String^ GetCommandLine(System::Diagnostics::Process^ process)
{
	System::Text::StringBuilder^ commandLine = gcnew System::Text::StringBuilder();
	//commandLine->Append(" ");	
	{
		auto searcher = gcnew System::Management::ManagementObjectSearcher("SELECT CommandLine FROM Win32_Process WHERE ProcessId = " + process->Id);
		for each(System::Management::ManagementBaseObject^ obj in searcher->Get())
		{
			commandLine->Append(obj["CommandLine"]);
			commandLine->Append(" ");
		}
	}
	return commandLine->ToString();
}


bool IsUserAdministrator()
{
	//bool value to hold our return value
	bool isAdmin;
	try
	{
		//get the currently logged in user
		WindowsIdentity^ user = WindowsIdentity::GetCurrent();
		WindowsPrincipal^ principal = gcnew WindowsPrincipal(user);
		isAdmin = principal->IsInRole(WindowsBuiltInRole::Administrator);
	}
	catch (UnauthorizedAccessException^ ex)
	{
		Trace::WriteLine("current user is not administrator " + ex->ToString());
		isAdmin = false;
	}
	catch (Exception^ ex)
	{
		Trace::WriteLine("current user is not administrator " + ex->ToString());
		isAdmin = false;
	}
	return isAdmin;
}

String^ createXML(String^ exePath, String^ param)
{
	String^ savePath = String::Empty;
	try
	{
		XmlDocument^ fdxml = gcnew XmlDocument();
		savePath = System::IO::Path::GetTempFileName();
		XmlNode^ rootNode = fdxml->CreateNode(XmlNodeType::Element, "runexe", nullptr);
		fdxml->AppendChild(rootNode);
		XmlNode^ exeNode = fdxml->CreateNode(XmlNodeType::Element, "exepath", nullptr);
		exeNode->InnerText = exePath;
		rootNode->AppendChild(exeNode);
		XmlNode^ paramNode = fdxml->CreateNode(XmlNodeType::Element, "parameter", nullptr);
		paramNode->InnerText = param;
		rootNode->AppendChild(paramNode);
		Trace::WriteLine("WPDController will save runxml to " + savePath);
		Trace::WriteLine(fdxml->OuterXml);
		fdxml->Save(savePath);
	}
	catch (Exception^ ex)
	{
		Trace::WriteLine("createXML exception " + ex->ToString());
	}
	return savePath;
}

int call_RMDU(String^ exeFilename, String^ args)
{
	int ret = -1;
	String^ xmlPath = createXML(exeFilename, args);
	try{
		String^ runRMDU = Path::Combine(Environment::GetEnvironmentVariable("APSTHOME"), "FDU.exe");
		if (File::Exists(xmlPath) && File::Exists(runRMDU))
		{
			Process^ myProcess = gcnew Process();
			//ProcessStartInfo^ startInfo = gcnew ProcessStartInfo();
			myProcess->StartInfo->FileName = runRMDU;
			myProcess->StartInfo->Arguments = String::Format("\"{0}\"", xmlPath);
			myProcess->StartInfo->WindowStyle = ProcessWindowStyle::Hidden;
			myProcess->StartInfo->CreateNoWindow = true;
			myProcess->StartInfo->UseShellExecute = false;
			myProcess->Start();
			myProcess->WaitForExit(5000);
			ret = myProcess->ExitCode;

		}
	}
	catch (Exception^ ex)
	{
		Trace::WriteLine(ex->ToString());
		ret = -1;
	}
	finally
	{
		File::Delete(xmlPath);
	}
	return ret;

}

[STAThread]
int main(array<System::String ^> ^args)
{ 
	array<System::Diagnostics::Process^>^ plist = System::Diagnostics::Process::GetProcessesByName("mmc");
	for each (auto p in plist)
	{
		String^ s = GetCommandLine(p);
		System::Diagnostics::Trace::WriteLine(s);
		if (s->IndexOf("gpedit.msc") > 0)
			p->Kill();
	}

	StringBuilder^ sb = gcnew StringBuilder();
	for each (System::String ^ ss in args)
	{
		Trace::WriteLine(ss);
		sb->Append(String::Format("\"{0}\" ", ss));
	}

	System::Configuration::Install::InstallContext^ _param = gcnew System::Configuration::Install::InstallContext(nullptr, args);
	if (_param->IsParameterTrue("debug"))
	{
		Console::WriteLine("Waiting for debugging....");
		Console::ReadKey();
	}

	if (!IsUserAdministrator())
	{
		call_RMDU(Process::GetCurrentProcess()->MainModule->FileName, sb->ToString());
		return ERROR_EA_ACCESS_DENIED;
	}
	String^ sKey = String::Empty;
	if (_param->Parameters->ContainsKey("key"))
	{
		String^ sskey = _param->Parameters["key"];
		using namespace Runtime::InteropServices;
		const wchar_t* lpKey =
			(const wchar_t*)(Marshal::StringToHGlobalUni(sskey)).ToPointer();
		TCHAR csName[MAX_PATH] = { 0 };
		TCHAR csFileName[MAX_PATH] = { 0 };
		GetEnvironmentVariable(_T("APSTHOME"), csFileName, MAX_PATH);
		PathAppend(csFileName, _T("config.ini"));
		GetPrivateProfileString(_T("MTP_Devices"), lpKey, _T(""), csName, MAX_PATH, csFileName);
		sKey = gcnew String(csName);
	}

	if (!String::IsNullOrEmpty(sKey) || _param->IsParameterTrue("force"))
	{
		if (_param->IsParameterTrue("enable")||String::Compare(sKey,"enable", true)==0)
		{
			return SetGroupPolicy_WPD_Device(1, 1);
		}
		else if (_param->IsParameterTrue("notconfigured") || String::Compare(sKey, "notconfigured", true) == 0)
		{
			return SetGroupPolicy_WPD_Device(0, 1);
		}
		else if (_param->IsParameterTrue("disable") || String::Compare(sKey, "disable", true) == 0)
		{
			return SetGroupPolicy_WPD_Device(2, 0);
		}
		else
		{
			Trace::WriteLine("Parameter error.");
			return ERROR_INVALID_PARAMETER;
		}
	}
	else
	{
		Trace::WriteLine("WPDController will not run. not configuration.");
		return 0;
	}


	Console::WriteLine("-enable    WPD Device Deny access.");
	Console::WriteLine("-notconfigured    WPD Device Sytem Default.");
	Console::WriteLine("-disable    WPD Device permit access.");
	Console::WriteLine("-key    read from APSTHOME config.ini [MTP_Devices]");
	Console::WriteLine("-force    if not config file, MUST add -force run with enable/notcofigured/disable");

    return 1;
}
